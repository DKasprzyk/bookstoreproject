﻿using DataAccess.Entities;
using DataAccess.Repositories.ProductRepository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLogic.Services.ProductService
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task<IEnumerable<Product>> GetAllProducts()
        {
            return await _productRepository.GetAllAsync();
        }

        public async Task<Product> GetProductById(int productId)
        {
            return await _productRepository.GetByIdAsync(productId);
        }

        public async Task AddProduct(Product product)
        {
            await _productRepository.AddAsync(product);
        }

        public async Task RemoveProduct(int productId)
        {
            await _productRepository.RemoveAsync(productId);
        }

        public async Task UpdateProduct(Product product)
        {
            await _productRepository.UpdateAsync(product);
        }
    }
}
