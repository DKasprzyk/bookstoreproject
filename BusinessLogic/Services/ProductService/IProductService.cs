﻿using DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLogic.Services.ProductService
{
    public interface IProductService : IService
    {
        Task<IEnumerable<Product>> GetAllProducts();

        Task<Product> GetProductById(int productId);

        Task AddProduct(Product product);

        Task RemoveProduct(int productId);

        Task UpdateProduct(Product product);
    }
}
