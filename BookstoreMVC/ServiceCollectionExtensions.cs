﻿using BusinessLogic.Services.ProductService;
using DataAccess;
using DataAccess.Repositories.ProductRepository;
using Microsoft.Extensions.DependencyInjection;

namespace BoookstoreMvc
{
    public static class ServiceCollectionExtensions
    {
        public static void AddServices(this IServiceCollection services)
        {
            services.AddScoped<IProductRepository, ProductRepository>();

            services.AddScoped<IProductService, ProductService>();

            services.AddScoped<DatabaseInitializer>();

            services.AddHttpContextAccessor();
        }
    }
}
