﻿using BookstoreMVC.Models;
using BookstoreMVC.Constants;
using BookstoreMVC.Extensions;
using BusinessLogic.Services.ProductService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using DataAccess.Entities;

namespace BookstoreMVC.Controllers
{
    public class CheckoutController : Controller
    {

        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;

        IProductService _productService;
        ShoppingCartViewModel _shoppingCart;

        public CheckoutController(IProductService productService, IHttpContextAccessor httpContextAccessor)
        {
            _productService = productService;

            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]
        public async Task<IActionResult> AddToCart(int productId)
        {
            var product = await _productService.GetProductById(productId);

            _shoppingCart = GetShoppingCart();
            if (_shoppingCart == null)
            {
                _shoppingCart = new ShoppingCartViewModel()
                {
                    Products = new List<Product>()
                };
            }

            _shoppingCart.Products.Add(product);
            UpdateShoppingCart(_shoppingCart);

            return RedirectToAction("List", "Product");
        }

        [HttpGet]
        public async Task<IActionResult> Cart()
        {
            _shoppingCart = GetShoppingCart();

            return View(_shoppingCart);
        }

        [HttpPost]
        public async Task<IActionResult> Cart(ShoppingCartViewModel shoppingCartView)
        {

            return View(_shoppingCart);
        }


        public void UpdateShoppingCart(ShoppingCartViewModel shoppingCart)
        {
            _session.SetComplexObject(SessionVariableNames.ShopingCart, shoppingCart);
        }

        public ShoppingCartViewModel GetShoppingCart()
        {
            return _session.GetComplexObject<ShoppingCartViewModel>(SessionVariableNames.ShopingCart);
        }
    }
}
