﻿using BusinessLogic.Services.ProductService;
using DataAccess.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BoookstoreMvc.Controllers
{
    public class ProductController : Controller
    {
        IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        public async Task<IActionResult> List()
        {
            var products = await _productService.GetAllProducts();

            return View(products);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> Create(Product product)
        {
            await _productService.AddProduct(product);

            return RedirectToAction("List");
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int productId)
        {
            await _productService.RemoveProduct(productId);

            return RedirectToAction("List");
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int productId)
        {
            var product = await _productService.GetProductById(productId);

            return View(product);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Product product)
        {
            await _productService.UpdateProduct(product);

            return RedirectToAction("List");
        }
    }
}
