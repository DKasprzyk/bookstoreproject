﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace BookstoreMVC.Extensions
{
    public static class SessionExtensions
    {
        public static void SetComplexObject<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T GetComplexObject<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default : JsonConvert.DeserializeObject<T>(value);
        }
    }
}
