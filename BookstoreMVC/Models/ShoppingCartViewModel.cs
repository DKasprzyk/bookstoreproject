﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BookstoreMVC.Models
{
    public class ShoppingCartViewModel
    {
        public List<Product> Products { get; set; }

        public decimal GetTotalPrice()
        {
            return Products.Sum(p => p.Price);
        }
    }
}
