﻿namespace BookstoreMVC.Constants
{
    public static class SessionVariableNames
    {
        public const string UserId = "_UserId";

        public const string ShopingCart = "_ShopingCartItemList";
    }
}
