﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace DataAccess
{
    public class DatabaseInitializer
    {
        private readonly IServiceProvider _provider;
        private readonly IConfiguration _configuration;

        public DatabaseInitializer(IServiceProvider provider, IConfiguration configuration)
        {
            _provider = provider;
            _configuration = configuration;
        }

        public void Initialize()
        {
            using (var ctx = _provider.GetService<BookstoreDataContext>())
            {
                ctx.Database.Migrate();
            }
        }
    }
}
