﻿namespace DataAccess.Common
{
    class RoleNames
    {
        public static string Admin = "Admin";
        public static string Guest = "Guest";
        public static string Member = "Member";
        public static string Premium = "Premium";
    }
}
