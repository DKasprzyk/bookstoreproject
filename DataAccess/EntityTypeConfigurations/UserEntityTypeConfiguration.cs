﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.EntityTypeConfigurations
{
    class UserEntityTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder
               .HasOne(u => u.UserRole)
               .WithMany(ur => ur.Users)
               .HasForeignKey(u => u.UserRoleId)
               .OnDelete(DeleteBehavior.SetNull);

            builder
               .HasMany(u => u.UserProducts)
               .WithOne(up => up.User)
               .HasForeignKey(up => up.UserId)
               .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
