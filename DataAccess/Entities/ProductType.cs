﻿using System.Collections.Generic;

namespace DataAccess.Entities
{
    public class ProductType : BaseEntity
    {
        public ProductType()
        {
            Products = new HashSet<Product>();
        }

        public string Name { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
