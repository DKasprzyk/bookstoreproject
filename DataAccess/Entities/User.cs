﻿using System.Collections.Generic;

namespace DataAccess.Entities
{
    public class User : BaseEntity
    {
        public User()
        {
            UserProducts = new HashSet<UserProduct>();
        }

        public string Name { get; set; }
        public int? UserRoleId { get; set; }


        public virtual UserRole UserRole { get; set; }
        public virtual ICollection<UserProduct> UserProducts { get; set; }
    }
}
