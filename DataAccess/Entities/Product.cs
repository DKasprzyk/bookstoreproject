﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Entities
{
    public class Product : BaseEntity
    {
        public Product()
        {
            UserProducts = new HashSet<UserProduct>();
        }

        public string Name { get; set; }

        [Column(TypeName = "decimal(9,2)")]
        public decimal Price { get; set; }
        public int? ProductTypeId { get; set; }

        public virtual ProductType ProductType { get; set; }
        public virtual ICollection<UserProduct> UserProducts { get; set; }
    }
}
