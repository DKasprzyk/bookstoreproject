﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace DataAccess.Entities
{
    public class BaseEntityEqalityComparer<T> : IEqualityComparer<T> where T : BaseEntity
    {
        public bool Equals([AllowNull] T x, [AllowNull] T y)
        {
            if (x == null && y == null)
                return true;

            if (x == null || y == null)
                return false;

            return x.Id == y.Id;
        }

        public int GetHashCode([DisallowNull] T obj)
        {
            return obj.Id;
        }
    }
}
