﻿using System.Collections.Generic;

namespace DataAccess.Entities
{
    public class UserRole : BaseEntity
    {
        public UserRole()
        {
            Users = new HashSet<User>();
        }

        public string Name { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
