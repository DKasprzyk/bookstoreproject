﻿using DataAccess.Entities;
using DataAccess.Repositories.GenericRepository;

namespace DataAccess.Repositories.ProductRepository
{
    public interface IProductRepository : IGenericRepository<Product>
    {
    }
}
