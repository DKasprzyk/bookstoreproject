﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess.Repositories.GenericRepository
{
    public interface IGenericRepository<T>
    {
        Task<List<T>> GetAllAsync();

        Task<T> GetByIdAsync(int id);

        Task AddAsync(T entity);

        Task UpdateAsync(T entity);

        Task RemoveAsync(int id);

        IQueryable<T> GetAll();

        Task AddWithDetachAsync(T entity);
    }
}
