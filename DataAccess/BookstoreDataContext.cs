﻿using DataAccess.Common;
using DataAccess.Entities;
using DataAccess.EntityTypeConfigurations;
using Microsoft.EntityFrameworkCore;

namespace DataAccess
{
    public class BookstoreDataContext : DbContext
    {
        public BookstoreDataContext(DbContextOptions<BookstoreDataContext> options)
            : base(options)
        { }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserProduct> UserProducts { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductType> ProductTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ProductTypeEntityTypeConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(UserRoleEntityTypeConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ProductEntityTypeConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(UserProductEntityTypeConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(UserEntityTypeConfiguration).Assembly);

            //Filling basic data
            modelBuilder.Entity<UserRole>()
              .HasData(
                   new { Id = 1, Name = RoleNames.Admin },
                   new { Id = 2, Name = RoleNames.Guest },
                   new { Id = 3, Name = RoleNames.Member },
                   new { Id = 4, Name = RoleNames.Premium }
               );

            modelBuilder.Entity<ProductType>()
                .HasData(
                    new { Id = 1, Name = ProductTypeNames.Book }
                );

            modelBuilder.Entity<Product>()
                .HasData(
                    new { Id = 1, Name = "Product1", ProductTypeId = 1, Price = 100.00m },
                    new { Id = 2, Name = "Product2", ProductTypeId = 1, Price = 100.00m },
                    new { Id = 3, Name = "Product3", ProductTypeId = 1, Price = 99.99m },
                    new { Id = 4, Name = "Product4", ProductTypeId = 1, Price = 200.00m }
                );

            base.OnModelCreating(modelBuilder);
        }
    }
}
